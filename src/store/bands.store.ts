import { createContext } from 'react'
import { decorate, observable, computed } from 'mobx'

export type Band = {
  id: string,
  name: string,
  info?: string,
  genre?: string,
  preview?: string,
  members?: {
    name: string
  }
}

class BandsSelected {
  bandsSelected = new Map<string, Band>();
  bandSelect = (band: Band) => {
    if (this.bandsSelected.has(band.id)) {
      this.bandsSelected.delete(band.id)
    } else {
      this.bandsSelected.set(band.id, band)
    }
  }
}

decorate(BandsSelected, {
  bandsSelected: observable
})

export default createContext(new BandsSelected())