import { createContext } from 'react'
import { decorate, observable } from 'mobx'
import { Band } from './bands.store';

export type EventType = {
  id: string
  city: string
  start: string
  end: string
  band: Band
  concertLink: string
  ticketLink: string
  price: string
}

class EventSelected {
  eventSelected = new Map<string, EventType>();
  eventSelect = (event: EventType) => {
    if (this.eventSelected.has(event.id)) {
      this.eventSelected.delete(event.id)
    } else {
      this.eventSelected.set(event.id, event)
    }
  }
}

decorate(EventSelected, {
  eventSelect: observable
})

export default createContext(new EventSelected())