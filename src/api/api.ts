const url = 'http://localhost:3000';

export const queryFetch = (query: string, variables?: {}) => {
  console.log(variables)
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables }),
  }).then(resp => resp.json())
}