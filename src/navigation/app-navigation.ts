import { createStackNavigator, createAppContainer } from "react-navigation";
import UserEventsScreen from '../screens/user-events/user-events.screen';
import FindEventContainer from "../screens/find-event";
import SelectSingersContainer from '../screens/select-signers/select-singers.container';

const AppNavigator = createStackNavigator(
  {
    UserEvents: {
      screen: UserEventsScreen
    },
    FindEvent: {
      screen: FindEventContainer
    },
    SelectSingers: {
      screen: SelectSingersContainer
    }
  },
  {
    initialRouteName: "UserEvents"
  }
);

export default createAppContainer(AppNavigator);