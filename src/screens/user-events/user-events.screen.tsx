import React from 'react'
import { View} from 'react-native'
import SafeAreaComponent from '../../components/safe-area/safe-area.component';
import Text from '../../components/text/text';
import { Button } from 'react-native-material-ui';
import { NavigationScreenProps } from 'react-navigation';
import { s } from 'react-native-better-styles';

type Props = {
} & NavigationScreenProps

const UserEventsScreen = (props: Props) => {

  
  return (
    <SafeAreaComponent>
      <View style={[s.flx_i, s.jcc, s.aic, s.mb5]}>
        <Text style={[s.b, s.fs2, s.mb1]}>Hi my friend!</Text>
        <Text style={[s.mb1]}>
          Do you want to spend your weekend unforgettably?
        </Text>
        <Text style={[s.mb1]}>
          Okay let's start to find something special for you!
        </Text>
        <Button onPress={() => props.navigation.navigate('FindEvent')} primary raised text="Find Concert" />
      </View>
    </SafeAreaComponent>
  )
}

export default UserEventsScreen
