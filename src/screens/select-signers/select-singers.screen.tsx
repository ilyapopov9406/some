import React from 'react'
import { View, Image, ActivityIndicator } from 'react-native'
import Button from '../../components/button/button.component';
import { NavigationScreenProps } from 'react-navigation';
import SafeAreaComponent from '../../components/safe-area/safe-area.component';
import { TextInput, FlatList } from 'react-native-gesture-handler';
import { Band } from '../../store/bands.store';
import { RemoteData } from '../../utils/useRemoteData';
import { PENDING } from '../../utils/statuses';
import { s, colors, sizes } from 'react-native-better-styles';
import { Images } from '../../assets/index';
import Text from '../../components/text/text';
import { Icon } from 'react-native-material-ui';

type Props = {
  setSearch: React.Dispatch<React.SetStateAction<string>>
  bandsResp: RemoteData
  search: string
  bandSelect: (band: Band) => void
  bandsSelected: Map<string, Band>
} & NavigationScreenProps

const keyExtractor = ({band}: {band:Band}) => band.id;
const BandPreview = ({ 
  item, 
  index 
} : { 
  item: {
    band: Band, 
    bandSelect: (band: Band) => void, 
    selected: boolean
  }, 
  index: number
}) =>  (
  <Button
    onPress={() => {item.bandSelect(item.band)}} 
    style={[s.bbw1, s.pv1, s.ph05, s.flx_row, s.aic, s.jcsb, { borderColor: colors.black }]}
  >
    <View style={[s.flx_i, s.flx_row, s.aic,]}>
      <View style={[s.h25, s.w25, s.mr075]}>
        <Image 
          source={{uri: item.band.preview}} 
          defaultSource={Images.Default}
          borderRadius={sizes['075']}
          style={[s.flx_i]}
        />
      </View>
      <Text>
        {item.band.name}
      </Text>
    </View>
    {
      item.selected ? <Icon name="done"/> : <Icon name="add"/>
    }
  </Button>
)

const SelectSingersScreen = ({bandsResp, setSearch, search, bandSelect, bandsSelected}: Props) => {
  const bands = bandsResp.data && (
    search ? 
      bandsResp.data.getBands.filter((band: Band) => band.name.indexOf(search) !== -1) : 
      bandsResp.data.getBands
  ) 
  return (
    <SafeAreaComponent>
      <TextInput 
        onChangeText={setSearch} 
        style={[s.bbw1, s.pv1, s.ph05, { borderColor: colors.black }, s.fs1]}
        placeholder='Search'
        placeholderTextColor={colors.grey}
      />
      <FlatList
        data={bands && bands.map((band: Band) => ({
          band: band,
          bandSelect: bandSelect,
          selected: bandsSelected.has(band.id)
        }))}
        keyExtractor={keyExtractor}
        renderItem={BandPreview}
        ListEmptyComponent={
          bandsResp.status === PENDING ? 
            <ActivityIndicator/> : 
            <Text style={[s.pv1, s.ph05, s.flx_i]}>No Singers found</Text>
        }
      />
    </SafeAreaComponent>
  )
}

export default SelectSingersScreen
