import React, { useState, useContext } from 'react'
import { View, Text } from 'react-native'
import SelectSingersScreen from './select-singers.screen';
import { NavigationScreenProps } from 'react-navigation';
import useRemoteData from '../../utils/useRemoteData';
import { queryFetch } from '../../api/api';
import { observer } from 'mobx-react';
import Button from '../../components/button/button.component';
import { values } from 'mobx';
import bandsStore from '../../store/bands.store';

type Props = {
} & NavigationScreenProps

const query = `query GetBands {
  getBands {
    id
    name
    info
    genre
    preview
    members {
      name
    }
  }
}`

const bandsFetch = () => queryFetch(query);

const SelectSingersContainer = ({...props}: Props) => {
  const store = useContext(bandsStore);
  const [resp] = useRemoteData({ loadData: bandsFetch })
  const [search, setSearch] = useState('');
  console.log(values(store.bandsSelected))
  return <SelectSingersScreen 
    {...props} 
    bandSelect={store.bandSelect}
    bandsSelected={store.bandsSelected}
    bandsResp={resp} 
    search={search} 
    setSearch={setSearch}
  />
}

SelectSingersContainer.navigationOptions = (props: Props) => {
  return {
    title: 'Singers',
  };
};

export default observer(SelectSingersContainer);
