import React, { useState } from 'react'
import { View, ActivityIndicator } from 'react-native'
import SafeAreaComponent from '../../components/safe-area/safe-area.component';
import Text from '../../components/text/text';
import { NavigationScreenProps, FlatList } from 'react-navigation';
import DateTimePicker from "react-native-modal-datetime-picker";
import Button from '../../components/button/button.component';
import { Icon, Button as ButtonMaterial } from 'react-native-material-ui';
import { Band } from '../../store/bands.store';
import { DataPickerInit } from './find-event.container';
import moment from 'moment';
import { s, colors } from 'react-native-better-styles';
import { PENDING } from '../../utils/statuses';
import EventsListComponent from './events-list.component';
import { values } from 'mobx';
import { TextInput } from 'react-native-gesture-handler';
import { EventType } from '../../store/event.store';

const today = new Date();

type Props = {
  bandsSelected: Band[]
  startData: {
    value: Date, 
    change: React.Dispatch<React.SetStateAction<Date>>
  } 
  endData: {
    value: Date, 
    change: React.Dispatch<React.SetStateAction<Date>>
  }
  datePickerData: {
    value: DataPickerInit, 
    change: React.Dispatch<React.SetStateAction<null | DataPickerInit>>
  }
  cityData: {
    value: string, 
    change: React.Dispatch<React.SetStateAction<string>>
  }
  budgetData: {
    value: string, 
    change: React.Dispatch<React.SetStateAction<string>>
  }
  showListData: {
    value: boolean, 
    change: React.Dispatch<React.SetStateAction<boolean>>
  }
  eventSelect: (event: EventType) => void
} & NavigationScreenProps

const FindEventScreen = ({ 
  bandsSelected, 
  navigation, 
  startData, 
  endData, 
  datePickerData, 
  cityData, 
  budgetData,
  showListData,
  eventSelect,
  bandIds
} : Props) => {

  return (
    <SafeAreaComponent>
      <View style={[s.flx_row, s.jcsb, s.aic, s.mb075]}>
        <Button 
          onPress={() => datePickerData.change({
            date: startData.value, 
            onConfirm: (date: Date) => {datePickerData.change(null); startData.change(date);}
          })}
          style={[s.pv025, s.ph05]}
        >
          <Text style={[s.b]}>Select start date:</Text>
          <Text>{moment(startData.value).format('MM/DD/YYYY')}</Text>
        </Button>
        <Button 
          onPress={() => datePickerData.change({
            date: endData.value, 
            onConfirm: (date: Date) => {
              datePickerData.change(null); 
              endData.change(date.getTime() >= endData.value.getTime() ? date : startData.value);
            
            }
          })}
          style={[s.pv025, s.ph05]}
        >
          <Text style={[s.b]}>Select end date:</Text>
          <Text>{moment(endData.value).format('MM/DD/YYYY')}</Text>
        </Button>
      </View>
      <View style={[s.flx_row, s.aic, s.jcsa, s.mb075]}>
        <TextInput 
          onChangeText={(data) => {cityData.change(data);}} 
          style={[s.bbw1, s.pv025, s.ph05, { borderColor: colors.black }, s.fs1, s.w8]}
          placeholder='City'
          placeholderTextColor={colors.grey}
        />
        <TextInput 
          onChangeText={(data) => {budgetData.change(data);}} 
          style={[s.bbw1, s.pv025, s.ph05, { borderColor: colors.black}, s.fs1, s.w8]}
          placeholder='Budget'
          placeholderTextColor={colors.grey}
        />
      </View>
      <View style={[s.flx_wrap, s.flx_row, s.aic, s.mb075]}>
        <Text>Selected Singers:</Text>
        {
          bandsSelected.map(band => <Text key={band.id} style={[s.ph05, s.pv025, s.bg_grey_60, s.br075, s.m025]}>{band.name}</Text>)
        }
        <Button onPress={() => navigation.navigate('SelectSingers')}>
          <Icon name="add"/>
        </Button>
      </View>
      <ButtonMaterial 
        disabled={!cityData.value || !budgetData.value || bandIds.length === 0} 
        onPress={() => {
          if (!showListData.value) { 
            showListData.change(true)
          }
        }} 
        primary 
        raised 
        text="Search" 
      />
      <>
        {
          showListData.value && (
            <EventsListComponent
              startDate={startData.value}
              endDate={endData.value}
              bandsSelected={bandIds}
              city={cityData.value}
              budget={budgetData.value}
              eventSelect={eventSelect}
            />
          )
        }
      </>
      <DateTimePicker
        isVisible={!!datePickerData.value}
        onConfirm={datePickerData.value ? datePickerData.value.onConfirm : () => {datePickerData.change(null);}}
        date={datePickerData.value ? datePickerData.value.date : today}
        onCancel={() => datePickerData.change(null)}
      />
    </SafeAreaComponent>
  )
};

export default FindEventScreen;