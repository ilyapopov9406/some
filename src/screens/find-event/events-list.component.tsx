import React from 'react'
import { View, ActivityIndicator, Image, Linking } from 'react-native'
import moment from 'moment';
import memoize from 'lodash/memoize'
import { queryFetch } from '../../api/api';
import useRemoteData from '../../utils/useRemoteData';
import { FlatList } from 'react-native-gesture-handler';
import { PENDING } from '../../utils/statuses';
import { EventType } from '../../store/event.store';
import { Band } from '../../store/bands.store';
import { s, sizes } from 'react-native-better-styles';
import Text from '../../components/text/text';
import { Images } from '../../assets';
import Button from '../../components/button/button.component';

const query = `query GetSuggestions(
  $start:String!,
  $end: String!,
  $bandIdList:[String]!,
  $city:String!,
  $budget:String!,
 ) {
  getSuggestions(
    start:$start
    end:$end
    bandIdList:$bandIdList
    city: $city
    budget:$budget
  ) {
    city
    start
    end
    concertLink
    ticketLink
    price
    band {
      name
      preview
    }
  }
 }`

const handleClick = (url: string = 'https://www.kiwi.com/u/x4z94f') => {
  Linking.canOpenURL('https://www.kiwi.com/u/x4z94f').then(supported => {
    if (supported) {
      Linking.openURL('https://www.kiwi.com/u/x4z94f');
    } else {
      console.log("Don't know how to open URI: " + 'https://www.kiwi.com/u/x4z94f');
    }
  });
};
const bandsFetch = (_variables) => queryFetch(query, _variables);
const keyExtractor = ({event}: {event: EventType}) => event.concertLink;
const EventView = ({
  item, 
  index
} : {
  item: {
    event: EventType, 
    eventSelect: (event: EventType) => void
  }, 
  index: number
}) => (
  <Button onPress={() => item.eventSelect(item.event)} style={[s.flx_row, s.aic, s.ph075, s.pv05, s.bg_grey_60, s.br1, s.mt1]}>
    <View style={[s.h25, s.w25, s.mr075]}>
      <Image
        source={{uri: item.event.band.preview}} 
        defaultSource={Images.Default}
        borderRadius={sizes['075']}
        style={[s.flx_i]}
      />
    </View>
    <View style={[s.ml05, s.aifs]}>
      <Text>Band: <Text style={[s.b]}>{item.event.band.name}</Text></Text>
      <Text>In city: <Text style={[s.b]}>{item.event.city}</Text></Text>
      <Text>Start at: <Text style={[s.b]}>{item.event.start}</Text></Text>
      <Text>End at: <Text style={[s.b]}>{item.event.end}</Text></Text>
      <Text>Price: <Text style={[s.b]}>${item.event.price}</Text></Text>
      <Button onPress={() => handleClick(item.event.concertLink)} style={[s.aifs]}>
        <Text style={[s.blue]}>Concert Link</Text>
      </Button>
      <Button onPress={() => handleClick(item.event.ticketLink)} style={[s.aifs]}>
        <Text style={[s.blue]}>Ticket Link</Text>
      </Button>
    </View>
  </Button>
)

type Props = {
  startDate: Date
  endDate: Date
  bandsSelected: Band[]
  city: string
  budget: string
  eventSelect: (event: EventType) => void
}

const generateVariables = memoize((
  startDate,
  endDate,
  bandsSelected,
  city,
  budget,
) => {
  return [{
    start: moment(startDate).format('MM/DD/YYYY'),
    end: moment(endDate).format('MM/DD/YYYY'),
    bandIdList: bandsSelected,
    city: city,
    budget: budget
  }]
})

const EventsListComponent = ({startDate, endDate, bandsSelected, city, budget, eventSelect}: Props) => {
  const variables = generateVariables(
    startDate,
    endDate,
    bandsSelected,
    city,
    budget,
  )

  const [resp] = useRemoteData({ loadData: bandsFetch, args: variables })
  console.log(resp)
  const events = resp.data && resp.data.getSuggestions;
  return (
    <FlatList
      data={events && events.map((event: EventType) => ({
        event,
        eventSelect
      }))}
      keyExtractor={keyExtractor}
      renderItem={EventView}
      ListEmptyComponent={
        resp.status === PENDING ? 
          <ActivityIndicator/> : 
          <Text style={[s.pv1, s.ph05, s.flx_i]}>No Events found</Text>
      }
    />
  )
}

export default EventsListComponent
