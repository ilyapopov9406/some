import React, { useEffect, useContext, useState }  from 'react';
import FindEventScreen from './find-event.screen';
import useRemoteData from '../../utils/useRemoteData';
import { NavigationScreenProps } from 'react-navigation';
import { queryFetch } from '../../api/api';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react-lite'
import get from 'lodash/get';
import { values } from 'mobx';
import BandsStore from '../../store/bands.store';
import EventStore from '../../store/event.store';

type Props = {
} & NavigationScreenProps

const today = new Date();
export type DataPickerInit = null | {
  date: Date
  onConfirm: (date: Date) => void
}
const datePickerInfoInit: DataPickerInit = null

const FindEventContainer = ({...props}: Props) => {
  const bandStore = useContext(BandsStore);
  const eventStore = useContext(EventStore);
  const [startDate, setStartDate] = useState(today);
  const [endDate, setEndDate] = useState(today);
  const [datePickerData, setDatePickerData] = useState(datePickerInfoInit);
  const [city, setCity] = useState('');
  const [budget, setBudget] = useState('');
  const [showList, setShowList] = useState(false);
  const bandIds = [];
  bandStore.bandsSelected.forEach(band => bandIds.push(band.id));
  return <FindEventScreen 
    startData={{value: startDate, change: setStartDate}} 
    endData={{value: endDate, change: setEndDate}}
    datePickerData={{value: datePickerData, change: setDatePickerData}}
    cityData={{value: city, change: setCity}} 
    budgetData={{value: budget, change: setBudget}}
    showListData={{value: showList, change: setShowList}}
    {...props} 
    bandsSelected={values(bandStore.bandsSelected)}
    bandIds={bandIds}
    eventSelect={eventStore.eventSelect}
  />;
}

FindEventContainer.navigationOptions = {
  title: 'Find Event',
}

export default observer(FindEventContainer);
