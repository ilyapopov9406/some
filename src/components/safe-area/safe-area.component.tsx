import React from 'react'
import { StatusBar, StyleProp, ViewStyle } from 'react-native'
import { SafeAreaView } from 'react-navigation';
import { s, colors } from 'react-native-better-styles';

const SafeAreaComponent = (
  {
    children, 
    styles
  } : {
    children: JSX.Element | JSX.Element[], 
    styles?: StyleProp<ViewStyle>
  }
) => {
  return (
    <SafeAreaView style={[s.flx_i, s.bg_shadow_70, s.p075, styles ? styles : []]}>
      <StatusBar
        barStyle="default"
        backgroundColor={colors.shadow_70}
      />
      {children}
    </SafeAreaView>
  )
}

export default SafeAreaComponent
