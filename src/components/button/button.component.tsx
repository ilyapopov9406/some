import React, { PureComponent } from 'react';
import Text from '../text/text';
import {
  Platform,
  StyleProp,
  TextStyle,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import { s } from 'react-native-better-styles';

const ButtonElement = Platform.select({
  ios: TouchableOpacity,
  android: TouchableNativeFeedback as any,
});

const rippleAndroid = Platform.select({
  android:
    Platform.Version >= 21
      ? TouchableNativeFeedback.Ripple('ThemeAttrAndroid', true)
      : TouchableNativeFeedback.SelectableBackground(),
});

type Props = {
  useBackgroundRipple?: boolean;
  onPress: () => void;
  title?: string;
  children?: JSX.Element | JSX.Element[];
  disabled?: boolean;
  primary?: boolean;
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
}

const Button = ({
  useBackgroundRipple, 
  onPress, 
  title, 
  children, 
  disabled, 
  primary, 
  style, 
  textStyle
}: Props) => (
  <ButtonElement
    onPress={onPress}
    disabled={disabled}
    background={useBackgroundRipple ? rippleAndroid : undefined}
  >
    <View
      style={[
        // primary && style.button,
        // disabled && style.disabled,
        style,
      ]}
    >
      {children || (
        <Text
          style={[
            // primary && style.text,
            // disabled && style.disabledText,
            textStyle,
          ]}
        >
          {title}
        </Text>
      )}
    </View>
  </ButtonElement>
)

export default Button;
