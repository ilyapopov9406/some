import React from 'react'
import { Text as TextElement, TextProps } from 'react-native'
import { s } from 'react-native-better-styles';

type ChildrenType = JSX.Element | string | undefined;

const Text = ({
  children,
  style, 
  ...props
}: (
  {children: Array<ChildrenType> | ChildrenType} &
  TextProps
)): JSX.Element => <TextElement style={[s.fs1, s.tc, s.black, style]} {...props}>{children}</TextElement>

export default Text