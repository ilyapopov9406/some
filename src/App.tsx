import React from 'react';
import AppContainer from './navigation/app-navigation';
import { ThemeContext, getTheme } from 'react-native-material-ui';
import './styles';
import { colors } from 'react-native-better-styles';
// import { RootStore } from './store/root';
// import { onPatch } from 'mobx-state-tree';
// import { Provider } from 'mobx-react';

const uiTheme = {
  palette: {
    primaryColor: colors.blue,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

// const store = RootStore.create({})
// onPatch(store, patch => {
//   console.log(patch);
// })

const App = () => {
  return (
  <ThemeContext.Provider value={getTheme(uiTheme)}>
    <AppContainer/>
  </ThemeContext.Provider>
)};

export default App;