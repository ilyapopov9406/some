import {
  useState,
  useEffect,
} from 'react';
import {
  PENDING,
  PRISTINE,
  SUCCESS,
  FAILURE,
  Status,
} from './statuses';

const emptyArr = new Array();
export type RemoteData = {
  status: Status,
  data: any,
  err: any
}

const useRemoteData = ({
  args = emptyArr,
  loadData,
} : {
  loadData: (...args: any) => Promise<any>, 
  args?: any[]
}) => {
  const state : RemoteData = {
    status: PRISTINE,
    data: null,
    err: null
  }
  
  const [ queryResult, setQueryResult ] = useState(state);

  useEffect(() => {
    setQueryResult({
      status: PENDING,
      data: null,
      err: null
    });
    loadData(...args).then(({data}) => {
      setQueryResult({
        status: SUCCESS,
        data,
        err: null
      });
      
      return data;
    }).catch((err) => {
      setQueryResult({
        status: FAILURE,
        err,
        data: null
      });
    });
  }, [ args, loadData ]);
  return [queryResult];
};

export default useRemoteData;