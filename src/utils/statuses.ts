export const PRISTINE = 'PRISTINE';
export const PENDING = 'PEDNING';
export const FAILURE = 'FAILURE';
export const SUCCESS = 'SUCCESS';
export type Status = 'PRISTINE' | 'PEDNING' | 'FAILURE' | 'SUCCESS';
